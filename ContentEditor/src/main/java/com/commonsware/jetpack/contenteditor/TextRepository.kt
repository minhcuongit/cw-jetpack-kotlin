/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.contenteditor

import android.content.ContentResolver
import android.content.Context
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Environment
import kotlinx.coroutines.*
import java.io.FileNotFoundException
import java.io.PrintWriter


object TextRepository {
  private val appScope = CoroutineScope(SupervisorJob())

  suspend fun read(context: Context, source: Uri) =
    withContext(Dispatchers.IO) {
      val resolver: ContentResolver = context.contentResolver

      try {
        resolver.openInputStream(source)?.use { stream ->
          StreamResult.Content(source, stream.reader().readText())
        } ?: throw IllegalStateException("could not open $source")
      } catch (e: FileNotFoundException) {
        StreamResult.Content(source, "")
      } catch (t: Throwable) {
        StreamResult.Error(t)
      }
    }

  suspend fun write(context: Context, source: Uri, text: String) =
    withContext(Dispatchers.IO + appScope.coroutineContext) {
      try {
        val resolver = context.contentResolver

        resolver.openOutputStream(source)?.let { os ->
          PrintWriter(os.writer()).use { out ->
            out.print(text)
            out.flush()
          }
        }

        val externalRoot =
          Environment.getExternalStorageDirectory().absolutePath

        if (source.scheme == "file" &&
          source.path!!.startsWith(externalRoot)
        ) {
          MediaScannerConnection
            .scanFile(
              context,
              arrayOf(source.path),
              arrayOf("text/plain"),
              null
            )
        }

        StreamResult.Content(source, text)
      } catch (t: Throwable) {
        StreamResult.Error(t)
      }
    }
}
