/*
  Copyright (c) 2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import androidx.core.os.bundleOf
import androidx.navigation.NavDeepLinkBuilder
import java.util.*

private const val ACTION_REFRESH = "refresh"
private const val EXTRA_APP_WIDGET_ID = "appWidgetId"

class ColorWidget : AppWidgetProvider() {
  private val random = Random()

  override fun onReceive(context: Context, intent: Intent) {
    if (intent.action == ACTION_REFRESH && intent.hasExtra(EXTRA_APP_WIDGET_ID)) {
      val appWidgetManager =
        context.getSystemService(AppWidgetManager::class.java)

      updateWidget(
        context,
        appWidgetManager,
        intent.getIntExtra(EXTRA_APP_WIDGET_ID, -1)
      )
    } else {
      super.onReceive(context, intent)
    }
  }

  override fun onUpdate(
    context: Context,
    appWidgetManager: AppWidgetManager,
    appWidgetIds: IntArray
  ) {
    appWidgetIds.forEach { updateWidget(context, appWidgetManager, it) }
  }

  private fun updateWidget(
    context: Context,
    appWidgetManager: AppWidgetManager,
    appWidgetId: Int
  ) {
    val remoteViews = RemoteViews(context.packageName, R.layout.widget)
    val color = random.nextInt()

    remoteViews.setTextViewText(
      R.id.label,
      context.getString(R.string.label_template, color)
    )
    remoteViews.setInt(R.id.root, "setBackgroundColor", color)

    val refreshIntent = Intent(context, ColorWidget::class.java)
      .setAction(ACTION_REFRESH)
      .putExtra(EXTRA_APP_WIDGET_ID, appWidgetId)
    val refreshPI = PendingIntent.getBroadcast(
      context,
      appWidgetId,
      refreshIntent,
      PendingIntent.FLAG_UPDATE_CURRENT
    )

    remoteViews.setOnClickPendingIntent(R.id.refresh, refreshPI)

    val deepLinkPI = NavDeepLinkBuilder(context)
      .setGraph(R.navigation.nav_graph)
      .setDestination(R.id.bigSwatchFragment)
      .setArguments(bundleOf("color" to color))
      .createPendingIntent()

    remoteViews.setOnClickPendingIntent(R.id.root, deepLinkPI)

    appWidgetManager.updateAppWidget(appWidgetId, remoteViews)
  }
}